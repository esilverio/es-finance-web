<?php

namespace ES\Finance\UIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQueryBuilder()
                    ->select('m')
                    ->from('ESFinanceDatabaseBundle:MenuGroup', 'm')
                    ->where('m.active = true')
                    ->orderBy('m.order', 'ASC')
                    ;
        $menu_groups = $query->getQuery()->getResult();
        
        return $this->render('ESFinanceUIBundle:Default:index.html.twig', 
                             array('menu_groups' => $menu_groups,
                                   'username' => $this->getUser()
                                  )
                            );
    }
    
    public function renderMenuAction($menu_group_id) {
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQueryBuilder()
                    ->select('m')
                    ->from('ESFinanceDatabaseBundle:Menu', 'm')
                    ->where('m.active = true')
                    ->andWhere('m.menuGroup = :menu_group_id')
                    ->setParameter('menu_group_id', $menu_group_id)
                    ->orderBy('m.order', 'ASC')
                    ;
        $menus = $query->getQuery()->getResult();
        
        return $this->render('ESFinanceUIBundle:Default:menu.html.twig', 
                             array('menus' => $menus
                                  )
                            );
    }
    
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error=$request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        else
        {
            $error=$session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        
        return $this->render('ESFinanceUIBundle:Default:login.html.twig',
                             array('username' => $session->get(SecurityContext::LAST_USERNAME),
                                   'error' => $error,
                                   'message' => SecurityContext::AUTHENTICATION_ERROR
                                  )
                            );
    }
}
