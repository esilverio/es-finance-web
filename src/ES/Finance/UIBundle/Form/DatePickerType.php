<?php

namespace ES\Finance\UIBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * DatePickerType
 *
 * @author esilverio
 */
class DatePickerType extends AbstractType 
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('widget' => 'single_text'));
    }
    
    public function getParent()
    {
        return 'date';
    }

    public function getName()
    {
        return 'datePicker';
    }
}
