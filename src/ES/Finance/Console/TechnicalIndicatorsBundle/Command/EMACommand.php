<?php

namespace ES\Finance\Console\TechnicalIndicatorsBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of EMACommand
 *
 * @author esilverio
 */
class EMACommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:ema')
             ->setDescription('Calculate Exponenital Moving Average for specified stock and period')
             ->addArgument('stock', InputArgument::REQUIRED, 'What\'s the stock to calculate?')
             ->addArgument('period', InputArgument::REQUIRED, 'What\'s the period?')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $count = 0;
        
        $symbol = $input->getArgument('stock');
        $period = $input->getArgument('period');
        
        if (!$symbol || !$period) {
            $output->writeln("Input data not specified!");
        }
        
        $helper = new QueryHelper();
        
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;
        
        $hps = $helper->get_historical_prices($symbol);
        
        $close_array = null;
        $date_array = null;
        foreach ($hps as $hp) {
            $close_array[] = $hp->getClose();
            $date_array[] = $hp->getDate();
        }
        
        $last_date = $this->getLastDate($symbol, $period);
        
        $data = trader_ema($close_array, $period);
        
        $stock = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                    ->findOneBy(array('symbol' => $symbol
                                     )
                    );
        
        $manager = $em->getManager();
        
        $i = 0;
        foreach($data as $element) {
            $date = $date_array[$i + $period - 1];
            $value = $element;

            if ($date->format('Y-m-d') >= $last_date) {
                $entity = $this->fillEma($date, $stock, $period, $value);
                
                $manager->persist($entity);
                $count++;
            }
            
            $i++;
        }
        
        $manager->flush();
        
        $output->writeln("Saved " . $count . " days!");
    }
    
    private function fillEma($date, $stock, $period, $value) {
        $entity = new \ES\Finance\DatabaseBundle\Entity\Ema();
        
        $entity->setStock($stock);
        $entity->setPeriod($period);
        $entity->setDate($date);
        $entity->setValue($value);
        
        return $entity;
    }
    
    private function getLastDate($symbol, $period) {
        $date = null;

        if ($symbol and $period)
        {
            $em = $this->getContainer()
                   ->get('doctrine')
                   ;
            
            $query = $em->getManager()
                        ->createQueryBuilder()
                        ->select('MAX(i.date)')
                        ->from('ESFinanceDatabaseBundle:Ema', 'i')
                        ->innerJoin('i.stock', 's')
                        ->where('s.symbol = :symbol')
                        ->setParameter('symbol', $symbol)
                        ->andWhere('i.period = :period')
                        ->setParameter('period', $period)
                        ;

            $result = $query->getQuery()
                            ->getSingleScalarResult()
                            ;
            
            if ($result)
            {
                $date = strtotime($result . ' + 1 day');
            }
        }

        if (!$date)
        {
            $date = mktime(0, 0, 0, 1, 1, 1900);
        }

        return date("Y-m-d", $date);
    }
}
