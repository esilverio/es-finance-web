<?php

namespace ES\Finance\Console\SignalsDetectorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ESFinanceConsoleSignalsDetectorBundle:Default:index.html.twig', array('name' => $name));
    }
}
