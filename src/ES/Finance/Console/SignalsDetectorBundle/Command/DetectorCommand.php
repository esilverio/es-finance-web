<?php

namespace ES\Finance\Console\SignalsDetectorBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Hoa\Ruler\Context;
use Hoa\Ruler\Ruler;

use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of DetectorCommand
 *
 * @author esilverio
 */
class DetectorCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:detect-signals')
             ->setDescription('Run signals detectors for all stocks')
             ->addArgument('stock', InputArgument::REQUIRED, 'Who is the stock you want to download?')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $symbol = $input->getArgument('stock');
        
        $helper = new QueryHelper();
        $helper->em = $this->getContainer()
                           ->get('doctrine')
                           ;
        
        $engine = new Ruler();
        $context = new Context();

        $rules = $helper->get_rules(null, null);
            
        $hps = $helper->get_historical_prices($symbol);
        foreach ($hps as $hp) {
            $date =  $hp->getDate();
            
            foreach ($rules as $r) {
                $rule = $r->getRule();
            
                $context = $this->getContext($rule, $symbol, $date);
                
                if ($engine->assert($rule, $context)) {
                    $output->writeln($date->format('Y-m-d') . " - " . $r->getMessage());
                }
            }
        }
    }

    private function getContext($rule, $symbol, $date) {
        $context = new Context();
        
        if (strpos($rule, 'ema14') >= 0) {
            $context['ema14'] = $this->getEMA($symbol, $date, 14);
        }
        if (strpos($rule, 'ema50') >= 0) {
            $context['ema50'] = $this->getEMA($symbol, $date, 50);
        }
        if (strpos($rule, 'ema200') >= 0) {
            $context['ema200'] = $this->getEMA($symbol, $date, 200);
        }
        if (strpos($rule, 'rsi14') >= 0) {
            $context['rsi14'] = $this->getRSI($symbol, $date, 14);
        }
        
        if (strpos($rule, 'sma14') >= 0) {
            $context['sma14'] = $this->getSMA($symbol, $date, 14);
        }
        if (strpos($rule, 'sma50') >= 0) {
            $context['sma50'] = $this->getSMA($symbol, $date, 50);
        }
        if (strpos($rule, 'sma200') >= 0) {
            $context['sma200'] = $this->getSMA($symbol, $date, 200);
        }
        
        if (strpos($rule, 'williams14') >= 0) {
            $context['williams14'] = $this->getWilliamsR($symbol, $date, 14);
        }
        
        return $context;
    }
    
    private function getSMA($symbol, $date, $period) {
        $helper = new QueryHelper();
        $helper->em = $this->getContainer()
                           ->get('doctrine')
                           ;

        return $helper->get_sma($symbol, $date, $period);
    }
    
    private function getEMA($symbol, $date, $period) {
        $helper = new QueryHelper();
        $helper->em = $this->getContainer()
                           ->get('doctrine')
                           ;

        return $helper->get_ema($symbol, $date, $period);
    }
    
    private function getRSI($symbol, $date, $period) {
        $helper = new QueryHelper();
        $helper->em = $this->getContainer()
                           ->get('doctrine')
                           ;

        return $helper->get_rsi($symbol, $date, $period);
    }
    
    private function getWilliamsR($symbol, $date, $period) {
        $helper = new QueryHelper();
        $helper->em = $this->getContainer()
                           ->get('doctrine')
                           ;

        return $helper->get_william_r($symbol, $date, $period);
    }
}
