<?php

namespace ES\Finance\Console\CandlesticksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ESFinanceConsoleCandlesticksBundle:Default:index.html.twig', array('name' => $name));
    }
}
