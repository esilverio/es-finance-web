<?php

namespace ES\Finance\Console\CandlesticksBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\DatabaseBundle\Entity\StockEvent;
use ES\Finance\DatabaseBundle\Entity\StockEventType;
use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of InvertedHammerCommand
 *
 * @author esilverio
 */
class InvertedHammerCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:candlesticks:inverted-hammer')
             ->setDescription('Detection of Candlestick Patterns - Inverted Hammer')
             ->addArgument('symbol', InputArgument::OPTIONAL, 'The symbol of the stock to be processed')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;
        
        $helper = new QueryHelper();
        $helper->em = $em;
        
        $symbol = $input->getArgument('symbol');
        
        $inverted_hammer = $helper->getStockEventType(StockEventType::CANDELSTICKS_INVERTED_HAMMER);
                        
        $manager = $em->getManager();
        
        $stocks = $helper->getStocks($symbol);
        foreach ($stocks as $stock) {
            $output->writeln($stock->getSymbol()); 
           
            $process = $helper->getLastProcessedDate($stock->getSymbol(), StockEventType::CANDELSTICKS_INVERTED_HAMMER);
            
            $days = 0;
            $patters = 0;
            
            $hps = $helper->getHistoricalPrices($stock->getSymbol(), $process->getLastDayProcessed());
            foreach ($hps as $hp) {
                if ($hp->getUp()) {
                    $high = $hp->getClose() + (2 * $hp->getIncrease());
                    $low1 = $hp->getOpen();
                    $low2 = $hp->getOpen() - ($hp->getIncrease() * 0.2);
                    
                    if ($hp->getHigh() >= $high) {
                        if ($hp->getLow() <= $low1 && $hp->getLow() >= $low2) {
                            $stock_event = new StockEvent();

                            $stock_event->setDate($hp->getDate());
                            $stock_event->setStock($stock);
                            $stock_event->setStockEventType($inverted_hammer);
                            $stock_event->setBearish(false);
                            $stock_event->setBullish(true);

                            $manager->persist($stock_event);
                            $patters++;
                        }
                    }
                }
                
                $process->setLastDayProcessed($hp->getDate());
                $days++;
            }
            
            $manager->persist($process);
            
            $hps = null;
            $process = null;
            
            $output->writeln($days . " days processed " . $patters . " patterns detected.");
        }
        
        $manager->flush();
    }
}
