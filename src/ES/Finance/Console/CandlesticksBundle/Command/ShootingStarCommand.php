<?php

namespace ES\Finance\Console\CandlesticksBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\DatabaseBundle\Entity\StockEvent;
use ES\Finance\DatabaseBundle\Entity\StockEventType;
use ES\Finance\DatabaseBundle\Entity\HistoricalPrice;
use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of ShootingStarCommand
 *
 * @author esilverio
 */
class ShootingStarCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:candlesticks:shooting-star')
             ->setDescription('Detection of Candlestick Patterns - Shooting Star')
             ->addArgument('symbol', InputArgument::OPTIONAL, 'The symbol of the stock to be processed')
             ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln("inicio");
        $output->writeln(round((memory_get_usage(true) / 1024 / 1024), 2) . " MB");
        
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;
            
        $helper = new QueryHelper();
        $helper->em = $em;

        $count = 0;
        $symbol = $input->getArgument('symbol');

        if ($symbol)
        {
            $output->writeln("one start");
            $output->writeln(round((memory_get_usage(true) / 1024 / 1024), 2) . " MB");
        
            $output->writeln($symbol);

            $stock = $helper->getStock($symbol, null);
            
            $count = $this->worker($symbol, $helper);
            $output->writeln("Finished (" . $count . " days processed.)");
            
            $output->writeln("one finish");
            $output->writeln(round((memory_get_usage(true) / 1024 / 1024), 2) . " MB");
        }
        else
        {
            $stocks = $helper->getStocks(null);
            foreach ($stocks as $stock)
            {
                $output->writeln($stock->getSymbol());
                
                $output->writeln("ciclo start");
                $output->writeln(round((memory_get_usage(true) / 1024 / 1024), 2) . " MB");

                $count = $this->worker($stock->getSymbol(), $helper);
                
                $output->writeln("ciclo finish");
                $output->writeln(round((memory_get_usage(true) / 1024 / 1024), 2) . " MB");
                
                $output->writeln("Finished (" . $count . " days processed.)");
            }
        }

        $output->writeln("End.");
    }
    
    private function worker($symbol, QueryHelper $helper)
    {
        $count = 0;
        $stock_event = null;
        
        $shooting_star = $helper->getStockEventType(StockEventType::CANDELSTICKS_SHOOTING_STAR);
        
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;

        $manager = $em->getManager();
        
        $stock = $helper->getStock($symbol, null);
        $process = $helper->getLastProcessedDate($stock->getSymbol(), StockEventType::CANDELSTICKS_SHOOTING_STAR);
        
        $hps = $helper->getHistoricalPrices($stock->getSymbol(), $process->getLastDayProcessed());
        foreach ($hps as $hp) {
            if ($this->check($hp)) {
                $stock_event = new StockEvent();

                $stock_event->setDate($hp->getDate())
                            ->setStock($stock)
                            ->setStockEventType($shooting_star)
                            ->setBearish(true)
                            ->setBullish(false)
                            ;

                $manager->persist($stock_event);
            }

            $process->setLastDayProcessed($hp->getDate());
            $manager->persist($process);
            
            $count++;
            
            unset($hp);
        }
        
        $manager->flush();
        $manager->clear();
        
        unset($stock_event);
        unset($process);
        unset($hps);
        
        unset($manager);
        
        return $count;
    }
    
    private function check(HistoricalPrice $hp) {
        $result = false;
        
        $high = $hp->getOpen() + (2 * $hp->getDecrease());
        $low1 = $hp->getClose();
        $low2 = $hp->getClose() - ($hp->getDecrease() * 0.2);
                
        if ($hp->getDown() && $hp->getHigh() >= $high && $hp->getLow() <= $low1 && $hp->getLow() >= $low2) {
            $result = true;
        }
        
        unset($high);
        unset($low1);
        unset($low2);
        
        unset($hp);
        
        return $result;
    }
}
