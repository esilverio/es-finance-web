<?php

namespace ES\Finance\Console\CandlesticksBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\DatabaseBundle\Entity\StockEvent;
use ES\Finance\DatabaseBundle\Entity\StockEventType;
use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of EngulfingCommand
 *
 * @author esilverio
 */
class EngulfingCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:candlesticks:engulfing')
             ->setDescription('Detection of Candlestick Patterns - Engulfing')
             ->addArgument('symbol', InputArgument::OPTIONAL, 'The symbol of the stock to be processed')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;
        
        $helper = new QueryHelper();
        $helper->em = $em;
        
        $symbol = $input->getArgument('symbol');
        
        $engulfing = $helper->getStockEventType(StockEventType::CANDELSTICKS_ENGULFING);
                        
        $manager = $em->getManager();
        
        $stocks = $helper->getStocks($symbol);
        foreach ($stocks as $stock) {
            $output->writeln($stock->getSymbol());
            
            $process = $helper->getLastProcessedDate($stock->getSymbol(), StockEventType::CANDELSTICKS_ENGULFING);
            
            $days = 0;
            $patters1 = 0;
            $patters2 = 0;
            
            $last_open = 0;
            $last_close = 0;
            $last_decrease = 0;
            $last_increase = 0;
            
            $last_down = false;
            $last_up = false;
            
            $hps = $helper->getHistoricalPrices($stock->getSymbol(), $process->getLastDayProcessed());
            foreach ($hps as $hp) {
                // Bearish - Baixa
                if ($last_up && $hp->getDown()) {
                    if ($hp->getDecrease() >= $last_increase * 2) {
                        if ($hp->getOpen() > $last_close && $hp->getClose() < $last_open) {
                            $stock_event = new StockEvent();

                            $stock_event->setDate($hp->getDate());
                            $stock_event->setStock($stock);
                            $stock_event->setStockEventType($engulfing);
                            $stock_event->setBearish(true);
                            $stock_event->setBullish(false);

                            $manager->persist($stock_event);
                            $patters1++;
                        }
                    }
                }

                // Bullish - Alta
                if ($last_down && $hp->getUp()) {
                    if ($hp->getIncrease() >= $last_decrease * 2) {
                        if ($hp->getOpen() < $last_close && $hp->getClose() > $last_open) {
                            $stock_event = new StockEvent();

                            $stock_event->setDate($hp->getDate());
                            $stock_event->setStock($stock);
                            $stock_event->setStockEventType($engulfing);
                            $stock_event->setBearish(false);
                            $stock_event->setBullish(true);

                            $manager->persist($stock_event);
                            $patters2++;
                        }
                    }
                }
                
                $last_open = $hp->getOpen();
                $last_close = $hp->getClose();
                
                $last_decrease = $hp->getDecrease();
                $last_increase = $hp->getIncrease();

                $last_down = $hp->getDown();
                $last_up = $hp->getUp();
                
                $process->setLastDayProcessed($hp->getDate());
                $days++;
            }
            
            $manager->persist($process);
            
            $hps = null;
            $process = null;
            
            $output->writeln($days . " days processed " . $patters1 . " bearish / " . $patters2 . " bullish detected.");
        }
        
        $manager->flush();
    }
}
