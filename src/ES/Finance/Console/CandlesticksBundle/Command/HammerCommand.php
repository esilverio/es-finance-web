<?php

namespace ES\Finance\Console\CandlesticksBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\DatabaseBundle\Entity\StockEvent;
use ES\Finance\DatabaseBundle\Entity\StockEventType;
use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of HammerCommand
 *
 * @author esilverio
 */
class HammerCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:candlesticks:hammer')
             ->setDescription('Detection of Candlestick Patterns - Hammer')
             ->addArgument('symbol', InputArgument::OPTIONAL, 'The symbol of the stock to be processed')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;
        
        $helper = new QueryHelper();
        $helper->em = $em;
        
        $symbol = $input->getArgument('symbol');
        
        $hammer = $helper->getStockEventType(StockEventType::CANDELSTICKS_HAMMER);
                        
        $manager = $em->getManager();
        
        
        $stocks = $helper->getStocks($symbol);
        foreach ($stocks as $stock) {
            $output->writeln($stock->getSymbol());
            
            $process = $helper->getLastProcessedDate($stock->getSymbol(), StockEventType::CANDELSTICKS_HAMMER);
            
            $days = 0;
            $patters = 0;
            
            $hps = $helper->getHistoricalPrices($stock->getSymbol(), $process->getLastDayProcessed());
            foreach ($hps as $hp) {
                if ($hp->getUp()) {
                    $low = $hp->getOpen() - (2 * $hp->getIncrease());
                    $high1 = $hp->getClose();
                    $high2 = $hp->getClose() + ($hp->getIncrease() * 0.2);
                    
                    if ($hp->getLow() <= $low) {
                        if ($hp->getHigh() >= $high1 && $hp->getHigh() <= $high2) {
                            $stock_event = new StockEvent();

                            $stock_event->setDate($hp->getDate());
                            $stock_event->setStock($stock);
                            $stock_event->setStockEventType($hammer);
                            $stock_event->setBearish(false);
                            $stock_event->setBullish(true);

                            $manager->persist($stock_event);
                            $patters++;
                        }
                    }
                }
                
                $process->setLastDayProcessed($hp->getDate());
                $days++;
            }
            
            $manager->persist($process);
            
            $hps = null;
            $process = null;
            
            $output->writeln($days . " days processed " . $patters . " patterns detected.");
        }
        
        $manager->flush();
    }
}
