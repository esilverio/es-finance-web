<?php

namespace ES\Finance\Console\CandlesticksBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\DatabaseBundle\Entity\StockEvent;
use ES\Finance\DatabaseBundle\Entity\StockEventType;
use ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper;

/**
 * Description of HaramiCommand
 *
 * @author esilverio
 */
class HaramiCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:candlesticks:harami')
             ->setDescription('Detection of Candlestick Patterns - Harami')
             ->addArgument('symbol', InputArgument::OPTIONAL, 'The symbol of the stock to be processed')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;
        
        $helper = new QueryHelper();
        $helper->em = $em;
        
        $symbol = $input->getArgument('symbol');
        
        $harami = $helper->getStockEventType(StockEventType::CANDELSTICKS_HARAMI);
                        
        $manager = $em->getManager();
        
        $output->writeln("1 - Usage: " . (memory_get_usage(true) / 1024 / 1024) . "Mb, Peak: " . (memory_get_peak_usage(true) / 1024 / 1024) . "Mb");
            
        $stocks = $helper->getStocks($symbol);
        foreach ($stocks as $stock) {
            $output->writeln($stock->getSymbol());
            
            $process = $helper->getLastProcessedDate($stock->getSymbol(), StockEventType::CANDELSTICKS_HARAMI);
            
            $days = 0;
            $patters1 = 0;
            $patters2 = 0;
            
            $last_open = 0;
            $last_close = 0;
            $last_decrease = 0;
            $last_increase = 0;
            
            $last_down = false;
            $last_up = false;
            
            $output->writeln("2 - Usage: " . (memory_get_usage(true) / 1024 / 1024) . "Mb, Peak: " . (memory_get_peak_usage(true) / 1024 / 1024) . "Mb");
            
            $hps = $helper->getHistoricalPrices($stock->getSymbol(), $process->getLastDayProcessed());
            foreach ($hps as $hp) {
                // Bearish - Baixa
                if ($last_up && $hp->getDown()) {
                    if ($hp->getDecrease() <= $last_increase / 2) {
                        if ($hp->getOpen() < $last_close && $hp->getClose() > $last_open) {
                            $stock_event = $this->fillEventType($hp->getDate(), $stock, $harami, true, false);

                            $manager->persist($stock_event);
                            $patters1++;
                            
                            unset($stock_event);
                        }
                    }
                }

                // Bullish - Alta
                if ($last_down && $hp->getUp()) {
                    if ($hp->getIncrease() <= $last_decrease / 2) {
                        if ($hp->getOpen() > $last_close && $hp->getClose() < $last_open) {
                            $stock_event = $this->fillEventType($hp->getDate(), $stock, $harami, false, true);

                            $manager->persist($stock_event);
                            $patters2++;
                            
                            unset($stock_event);
                        }
                    }
                }
                
                $last_open = $hp->getOpen();
                $last_close = $hp->getClose();
                
                $last_decrease = $hp->getDecrease();
                $last_increase = $hp->getIncrease();

                $last_down = $hp->getDown();
                $last_up = $hp->getUp();
                
                $process->setLastDayProcessed($hp->getDate());
                $days++;
                
                unset($hp);
            }
            
            $manager->persist($process);
            
            $manager->flush();
            $manager->clear();
            
            unset($hps);
            unset($process);
            
            unset($last_open);
            unset($last_close);
            unset($last_decrease);
            unset($last_increase);
            
            unset($last_down);
            unset($last_up);
        
            $output->writeln("3 - Usage: " . (memory_get_usage(true) / 1024 / 1024) . "Mb, Peak: " . (memory_get_peak_usage(true) / 1024 / 1024) . "Mb");
            $output->writeln($days . " days processed " . $patters1 . " bearish / " . $patters2 . " bullish detected.");
            
            unset($patters1);
            unset($patters2);
            
            unset($days);
        }
        
        $output->writeln("4 - Usage: " . (memory_get_usage(true) / 1024 / 1024) . "Mb, Peak: " . (memory_get_peak_usage(true) / 1024 / 1024) . "Mb");
    }
    
    private function fillEventType($date, $stock, $eventType, $bearish, $bullish) 
    {
        $stock_event = new StockEvent();

        $stock_event->setDate($date);
        $stock_event->setStock($stock);
        $stock_event->setStockEventType($eventType);
        $stock_event->setBearish($bearish);
        $stock_event->setBullish($bullish);
        
        return $stock_event;
    }
}
