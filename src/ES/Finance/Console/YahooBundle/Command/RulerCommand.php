<?php

namespace ES\Finance\Console\YahooBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of YahooCommand
 *
 * @author esilverio
 */
class RulerCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('yahoo:ruler')
             ->setDescription('Download Historical Data from Yahoo')
             ->addArgument('stock', InputArgument::OPTIONAL, 'Who is the stock you want to download?')
             ->addArgument('period', InputArgument::OPTIONAL)
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine');

        $count = 0;
        $symbol = $input->getArgument('stock');
        $period = $input->getArgument('period');
        
        if (!$period) {
            $period = 14;
        }
            
        if ($symbol)
        {
            $output->writeln($symbol);
            
            $query = $em->getManager()
                        ->createQueryBuilder()
                        ->select('hp')
                        ->from('ESFinanceDatabaseBundle:HistoricalPrice', 'hp')
                        ->innerJoin('hp.stock', 's')
                        ->where('s.symbol = :symbol')
                        ->setParameter('symbol', $symbol)
                        ->orderBy('hp.date', 'ASC')
                        ;
            
            $date = array();
            $close = array();

            $hp = $query->getQuery()
                        ->getResult()
                        ;
            
            foreach ($hp as $price)
            {
                $date[] = $price->getDate();
                $close[] = $price->getClose();
            }

            $rsi_data = trader_rsi($close, $period);
        
            $ruler = new \Hoa\Ruler\Ruler();
            $rule_buy = 'close < 30 and not buy';
            $rule_sell = 'close > 70 and not sell';
            
            $context = new \Hoa\Ruler\Context();
            
            $max = sizeof($rsi_data);
            $buy = false;
            $sell = false;
            
            #$date = array_reverse($date);
            #$rsi_data = array_reverse($rsi_data);
            
            $output->writeln(sizeof($date));
            $output->writeln(sizeof($close));
            $output->writeln(sizeof($rsi_data));
            
            $i = 0;
            foreach($rsi_data as $rsi)
            {
                $context['close'] = $rsi;
                $context['buy'] = $buy;
                $context['sell'] = $sell;
                
                $result1 = $ruler->assert($rule_buy, $context);
                if ($result1 === true)
                {
                    $output->writeln("Buy signal detect!");
                    $output->writeln($date[$i + $period - 1]->format('Y-m-d'));
                    $output->writeln($rsi);
                    
                    $buy = true;
                }
                else {
                    $buy = false;
                }
                
                $result2 = $ruler->assert($rule_sell, $context);
                if ($result2 === true)
                {
                    $output->writeln("Sell signal detect!");
                    $output->writeln($date[$i + $period - 1]->format('Y-m-d'));
                    $output->writeln($rsi);
                    
                    $sell = true;
                }
                else {
                    $sell = false;
                }
                
                $i++;
            }
            
            $sma_data = trader_sma($close, $period);
            
            $i = 0;
            foreach($sma_data as $sma) {
                
                $output->writeln($date[$i + $period - 1]->format('Y-m-d'));
                $output->writeln($close[$i + $period - 1]);
                $output->writeln($sma);
                
                $i++;
            }
        }
    }
}