<?php

namespace ES\Finance\Console\YahooBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use ES\Finance\Common\CsvHelper;
use ES\Finance\DatabaseBundle\Entity\HistoricalPrice;

/**
 * Description of YahooCommand
 *
 * @author esilverio
 */
class YahooCommand extends ContainerAwareCommand {
    protected function configure() {
        $this->setName('finance:yahoo')
             ->setDescription('Download Historical Data from Yahoo')
             ->addArgument('stock', InputArgument::OPTIONAL, 'Who is the stock you want to download?')
             ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()
                   ->get('doctrine')
                   ;

        $count = 0;
        $symbol = $input->getArgument('stock');

        if ($symbol)
        {
            $output->writeln($symbol);

            $count = $this->getHistoricalPrice($output, $em, $symbol);
            $output->writeln("Finished (" . $count . " days downloaded.)");
        }
        else
        {
            $stocks = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                         ->findByActive(TRUE)
                         ;

            foreach ($stocks as $stock)
            {
                $output->writeln($stock->getSymbol());

                $count = $this->getHistoricalPrice($output, $em, $stock->getSymbol());
                $output->writeln("Finished (" . $count . " days downloaded.)");

//                $output->writeln(round((memory_get_usage() / 1024 / 1024), 2) . " MB");
            }
        }

        $output->writeln("End.");
    }

    private function getHistoricalPrice(OutputInterface $output, $em, $symbol)
    {
        $count = 0;

        if (!$symbol)
            return $count;
        
        $stock = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                    ->findOneBy(array('symbol' => $symbol,
                                      'active' => '1'
                                     )
                    );
        
        if (!$stock)
            return $count;
        
        $helper = new CsvHelper();
        $last_update_date = $this->getLastDateOfHistoricalPrice($em, $symbol);

        if ($last_update_date == date('Y-m-d'))
            return $count;

        $csv = $helper->getCsv($symbol, $last_update_date);

        if (!$csv)
            return $count;

        $array_csv = $helper->csv_to_array($csv);

        if (count($array_csv) > 0)
        {
            $manager = $em->getManager();
            $array = array_reverse($array_csv);

            $batch_size = 2500;
            foreach ($array as $element)
            {
                $hp = $this->fillHistoricalPrice($element, $stock);
                $manager->persist($hp);

                $count++;

                if (($count % $batch_size) === 0)
                {
                    $manager->flush();
                    $manager->clear();

                    $stock = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                                ->findOneBySymbol($symbol)
                                ;
                }
            }

            $manager->flush();
            $manager->clear();

            unset($array);
            unset($manager);
        }

        unset($csv);
        unset($array_csv);
        unset($stock);
        unset($helper);

        return $count;
    }

    private function getLastDateOfHistoricalPrice($em, $symbol)
    {
        $date = null;

        if ($symbol)
        {
            $query = $em->getManager()
                        ->createQueryBuilder()
                        ->select('MAX(hp.date)')
                        ->from('ESFinanceDatabaseBundle:HistoricalPrice', 'hp')
                        ->innerJoin('hp.stock', 's')
                        ->where('s.symbol = :symbol')
                        ->setParameter('symbol', $symbol)
                        ;

            $result = $query->getQuery()
                            ->getSingleScalarResult()
                            ;
            
            if ($result)
            {
                $date = strtotime($result . ' + 1 day');
            }
        }

        if (!$date)
        {
            $date = mktime(0, 0, 0, 1, 1, 1900);
        }

        return date("Y-m-d", $date);
    }

    private function fillHistoricalPrice($data, $stock)
    {
        if ($data)
        {
            $hp = new HistoricalPrice();
            $hp->setStock($stock);
            $hp->setDate(new \DateTime($data['Date']));
            $hp->setOpen($data['Open']);
            $hp->setHigh($data['High']);
            $hp->setLow($data['Low']);
            $hp->setClose($data['Close']);
            
            if ($hp->getClose() === $hp->getOpen()) {
                $hp->setIncrease(0);
                $hp->setDecrease(0);
                
                $hp->setDown(false);
                $hp->setUp(false);
            }
            
            if ($hp->getClose() > $hp->getOpen()) {
                $hp->setIncrease($hp->getClose() - $hp->getOpen());
                $hp->setDecrease(0);
                
                $hp->setDown(false);
                $hp->setUp(true);
            }
            
            if ($hp->getClose() < $hp->getOpen()) {
                $hp->setIncrease(0);
                $hp->setDecrease($hp->getOpen() - $hp->getClose());
                
                $hp->setDown(true);
                $hp->setUp(false);
            }
            
            $hp->setVolume($data['Volume']);
            $hp->setAdjustedClose($data['Adj Close']);

            return $hp;
        }
        else
        {
            return null;
        }
    }
}
