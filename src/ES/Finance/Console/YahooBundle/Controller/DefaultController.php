<?php

namespace ES\Finance\Console\YahooBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function downloadAction($symbol) {
        $message = null;

        if ($symbol) {
            $count = $this->getHistoricalPrice($symbol);

            $message = $count . " days of historical data downloaded";
        } else {
            $message = "Please, specify the stock symbol";
        }

        return $this->render('FinanceYahooBundle:Default:index.html.twig', array('message' => $message));
    }

    public function downloadAllAction() {
        $message = null;

        $count = $this->getAllHistoricalPrice();

        $message = "Downloaded all of historical data. Count: " . $count;


        return $this->render('FinanceYahooBundle:Default:index.html.twig', array('message' => $message));
    }

    public function getHistoricalPrice($symbol) {
        $count = 0;

        if (!$symbol)
            return $count;

        $em = $this->getDoctrine();
        $stock = $em->getRepository('FinanceCommonBundle:Stock')->findOneBySymbol($symbol);

        if (!$stock)
            return $count;

        $helper = new CsvHelper();
        $csv = $helper->getCsv($symbol, $this->getLastDateOfHistoricalPrice($symbol));

        if ($csv) {
            $manager = $em->getManager();
            $array = array_reverse($helper->csv_to_array($csv));

            foreach ($array as $element) {
                $hp = $this->fillHistoricalPrice($element, $stock);
                $manager->persist($hp);
                $count++;
            }
            $manager->flush();
        }

        return $count;
    }

    private function getAllHistoricalPrice() {
        $count = 0;
        $em = $this->getDoctrine();
        $stocks = $em->getRepository('FinanceCommonBundle:Stock')->findAll();

        foreach ($stocks as $stock)
        {
            $count = $count + $this->getHistoricalPrice($stock->getSymbol());
        }

        return $count;
    }

    private function getLastDateOfHistoricalPrice($symbol) {
        $date = null;

        if ($symbol) {
            $em = $this->getDoctrine();
            $query = $em->getManager()->createQueryBuilder()
                        ->select('MAX(hp.date)')
                        ->from('FinanceCommonBundle:HistoricalPrice', 'hp')
                        ->innerJoin('hp.stock', 's')
                        ->where('s.symbol = :symbol')
                        ->setParameter('symbol', $symbol)
                        ;

            $result = $query->getQuery()->getSingleScalarResult();
            if ($result) {
                $date = strtotime($result . ' + 1 day');
            }
        }

        if (!$date) {
            $date = mktime(0, 0, 0, 1, 1, 1900);
        }

        return date("Y-m-d", $date);
    }

    private function fillHistoricalPrice($data, $stock) {
        if ($data) {
            $hp = new HistoricalPrice();
            $hp->setStock($stock);
            $hp->setDate(new \DateTime($data['Date']));
            $hp->setOpen($data['Open']);
            $hp->setHigh($data['High']);
            $hp->setLow($data['Low']);
            $hp->setClose($data['Close']);
            $hp->setVolume($data['Volume']);
            $hp->setAdjustedClose($data['Adj Close']);

            return $hp;
        } else {
            return null;
        }
    }
}
