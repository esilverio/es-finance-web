<?php

namespace ES\Finance\Common;

/**
 * Description of CsvHelper
 *
 * @author esilverio
 */
class CsvHelper
{
    public function getCsv($symbol, $lastdate)
    {
        $i = date_parse_from_format("Y-m-d", $lastdate);

        $day_i = $i["day"];
        $month_i = $i["month"] - 1;
        $year_i = $i["year"];

        $day_f = date("d");
        $month_f = date("m") - 1;
        $year_f = date("Y");
  
        $url = "http://real-chart.finance.yahoo.com/table.csv?"
             . "s=" . $symbol
             . "&a=" . $month_i
             . "&b=" . $day_i
             . "&c=" . $year_i
             . "&d=" . $month_f
             . "&e=" . $day_f
             . "&f=" . $year_f
             . "&g=d"
             . "&ignore=.csv"
             ;
        
        $csv = @file_get_contents($url);
        
        return $csv;
    }

    public function csv_to_array($csv, $delimiter = ',', $enclosure = '"', $escape = '\\', $terminator = "\n")
    {
        $r = array();

        $rows = explode($terminator, trim($csv));
        $names = str_getcsv(array_shift($rows), $delimiter, $enclosure, $escape);
        $nc = count($names);

        foreach ($rows as $row)
        {
            if (trim($row))
            {
                $values = str_getcsv($row, $delimiter, $enclosure, $escape);
                if (!$values)
                {
                    $values = array_fill(0, $nc, null);
                }

                $r[] = array_combine($names, $values);
            }
        }

        return $r;
    }
}
