<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ES\Finance\DatabaseBundle\Entity\Account;
use ES\Finance\DatabaseBundle\Form\AccountType;

/**
 * Account controller.
 *
 * @Route("/es_finance_accounts")
 */
class AccountController extends Controller
{
    /**
     * Lists all Account entities.
     *
     * @Route("/", name="es_finance_accounts")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ESFinanceDatabaseBundle:Account')
                       ->findAll()
                       ;

        return array('entities' => $entities);
    }

     /**
     * Creates a new Account entity.
     *
     * @Route("/", name="es_finance_accounts_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:Account:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Account();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_accounts_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a Account entity.
     *
     * @param Account $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Account $entity)
    {
        $form = $this->createForm(new AccountType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_accounts_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_accounts'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new Account entity.
     *
     * @Route("/new", name="es_finance_accounts_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Account();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a Account entity.
     *
     * @Route("/{id}", name="es_finance_accounts_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Account')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Account entity.
     *
     * @Route("/{id}/edit", name="es_finance_accounts_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Account')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a Account entity.
     *
     * @param Account $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Account $entity)
    {
        $form = $this->createForm(new AccountType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_accounts_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_accounts'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing Account entity.
     *
     * @Route("/{id}", name="es_finance_accounts_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:Account:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Account')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_accounts_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a Account entity.
     *
     * @Route("/{id}", name="es_finance_accounts_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:Account')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Account entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_accounts'));
    }

    /**
     * Creates a form to delete a Account entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_accounts_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_accounts'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
