<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ES\Finance\DatabaseBundle\Entity\Industry;
use ES\Finance\DatabaseBundle\Form\IndustryType;

/**
 * Industry controller.
 *
 * @Route("/es_finance_industries")
 */
class IndustryController extends Controller
{
    /**
     * Lists all Industry entities.
     *
     * @Route("/", name="es_finance_industries")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ESFinanceDatabaseBundle:Industry')
                       ->findAll()
                       ;

        return array('entities' => $entities);
    }

     /**
     * Creates a new Industry entity.
     *
     * @Route("/", name="es_finance_industries_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:Industry:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Industry();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_industries_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a Industry entity.
     *
     * @param Industry $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Industry $entity)
    {
        $form = $this->createForm(new IndustryType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_industries_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_industries'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new Industry entity.
     *
     * @Route("/new", name="es_finance_industries_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Industry();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a Industry entity.
     *
     * @Route("/{id}", name="es_finance_industries_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Industry')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Industry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Industry entity.
     *
     * @Route("/{id}/edit", name="es_finance_industries_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Industry')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Industry entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a Industry entity.
     *
     * @param Industry $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Industry $entity)
    {
        $form = $this->createForm(new IndustryType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_industries_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_industries'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing Industry entity.
     *
     * @Route("/{id}", name="es_finance_industries_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:Industry:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Industry')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Industry entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_industries_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a Industry entity.
     *
     * @Route("/{id}", name="es_finance_industries_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:Industry')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Industry entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_industries'));
    }

    /**
     * Creates a form to delete a Industry entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_industries_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_industries'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
