<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ES\Finance\DatabaseBundle\Entity\StockSale;
use ES\Finance\DatabaseBundle\Form\StockSaleType;

/**
 * StockSale controller.
 *
 * @Route("/es_finance_stockssales")
 */
class StockSaleController extends Controller
{
    /**
     * Lists all StockSale entities.
     *
     * @Route("/", name="es_finance_stockssales")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ESFinanceDatabaseBundle:StockSale')
                       ->findAll()
                       ;

        return array('entities' => $entities);
    }

     /**
     * Creates a new StockSale entity.
     *
     * @Route("/", name="es_finance_stockssales_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:StockSale:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new StockSale();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_stockssales_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a StockSale entity.
     *
     * @param StockSale $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(StockSale $entity)
    {
        $form = $this->createForm(new StockSaleType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_stockssales_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_stockssales'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new StockSale entity.
     *
     * @Route("/new", name="es_finance_stockssales_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new StockSale();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a StockSale entity.
     *
     * @Route("/{id}", name="es_finance_stockssales_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:StockSale')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockSale entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing StockSale entity.
     *
     * @Route("/{id}/edit", name="es_finance_stockssales_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:StockSale')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockSale entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a StockSale entity.
     *
     * @param StockSale $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(StockSale $entity)
    {
        $form = $this->createForm(new StockSaleType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_stockssales_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_stockssales'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing StockSale entity.
     *
     * @Route("/{id}", name="es_finance_stockssales_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:StockSale:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:StockSale')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockSale entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_stockssales_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a StockSale entity.
     *
     * @Route("/{id}", name="es_finance_stockssales_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:StockSale')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find StockSale entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_stockssales'));
    }

    /**
     * Creates a form to delete a StockSale entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_stockssales_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_stockssales'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
