<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Description of PortfolioController
 *
 * @author esilverio
 */
class PortfolioController extends Controller {
    /**
     * Portfolioo overview.
     *
     * @Route("/", name="es_finance_portfolios")
     * @Method("GET")
     * @Template()
     */
    public function viewAction() {
        $em = $this->getDoctrine()
                   ->getManager()
                   ;
        
        $query = $em->createQueryBuilder()
                    ->select('sbs')
                    ->from('ESFinanceDatabaseBundle:StockBySector', 'sbs')
                    ->where('sbs.user = :user')
                    ->setParameter('user', 1)
                    ->andWhere('sbs.quantity > 0')
                    ->orderBy('sbs.quantity', 'DESC')
                    ;
        
        $stocks_by_sector = $query->getQuery()
                                  ->getResult()
                                  ;
        
        
        $query = $em->createQueryBuilder()
                    ->select('sbi')
                    ->from('ESFinanceDatabaseBundle:StockByIndustry', 'sbi')
                    ->where('sbi.user = :user')
                    ->setParameter('user', 1)
                    ->andWhere('sbi.quantity > 0')
                    ->orderBy('sbi.quantity', 'DESC')
                    ;
        
        $stocks_by_industry = $query->getQuery()
                                    ->getResult()
                                    ;
        
        return array('sectors' => $stocks_by_sector,
                     'industries' => $stocks_by_industry
                    );
    }
}
