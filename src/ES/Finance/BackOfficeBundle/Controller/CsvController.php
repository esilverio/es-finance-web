<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of CsvController
 *
 * @author esilverio
 */
class CsvController extends Controller {
    public function downloadAction($id) {
        $em = $this->getDoctrine()
                   ->getManager()
                   ;
        
        $query = $em->createQueryBuilder()
                    ->select('s')
                    ->from('ESFinanceDatabaseBundle:Stock', 's')
                    ->where('s.id = :stock_id')
                    ->setParameter('stock_id', $id)
                    ;
        
        $stock = $query->getQuery()
                       ->getSingleResult()
                       ;
        
        if ( $stock ) {
            $filename = $stock->getSymbol() . ".csv"; 

            $query = $em->createQueryBuilder()
                        ->select('hp')
                        ->from('ESFinanceDatabaseBundle:HistoricalPrice', 'hp')
                        ->where('hp.stock = :stock_id')
                        ->setParameter('stock_id', $id)
                        ->orderBy('hp.date', 'ASC')
                        ;

            $entities = $query->getQuery()
                              ->getResult()
                              ;
        }
        
        $response = $this->render('ESFinanceBackOfficeBundle:Csv:download.html.twig', 
                                  array('entities' => $entities)
        );

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Description', 'Submissions Export');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $filename);
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');
        $response->sendHeaders();

        return $response; 
    }
}
