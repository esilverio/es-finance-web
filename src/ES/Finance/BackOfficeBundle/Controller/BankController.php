<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ES\Finance\DatabaseBundle\Entity\Bank;
use ES\Finance\DatabaseBundle\Form\BankType;

/**
 * Bank controller.
 *
 * @Route("/es_finance_banks")
 */
class BankController extends Controller
{
    /**
     * Lists all Bank entities.
     *
     * @Route("/", name="es_finance_banks")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ESFinanceDatabaseBundle:Bank')
                       ->findAll()
                       ;

        return array('entities' => $entities);
    }

     /**
     * Creates a new Bank entity.
     *
     * @Route("/", name="es_finance_banks_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:Bank:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Bank();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_banks_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a Bank entity.
     *
     * @param Bank $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bank $entity)
    {
        $form = $this->createForm(new BankType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_banks_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_banks'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new Bank entity.
     *
     * @Route("/new", name="es_finance_banks_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Bank();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a Bank entity.
     *
     * @Route("/{id}", name="es_finance_banks_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Bank')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bank entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Bank entity.
     *
     * @Route("/{id}/edit", name="es_finance_banks_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Bank')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bank entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a Bank entity.
     *
     * @param Bank $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Bank $entity)
    {
        $form = $this->createForm(new BankType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_banks_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_banks'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing Bank entity.
     *
     * @Route("/{id}", name="es_finance_banks_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:Bank:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Bank')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bank entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_banks_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a Bank entity.
     *
     * @Route("/{id}", name="es_finance_banks_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:Bank')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bank entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_banks'));
    }

    /**
     * Creates a form to delete a Bank entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_banks_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_banks'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
