<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Description of MyStocksController
 *
 * @author esilverio
 */
class MyStocksController extends Controller {
    /**
     * Lists all Stock entities.
     *
     * @Route("/", name="es_finance_mystocks")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()
                   ->getManager()
                   ;
        
        $query = $em->createQueryBuilder()
                    ->select('s')
                    ->from('ESFinanceDatabaseBundle:Stock', 's')
                    //->innerJoin('ESFinanceDatabaseBundle:StockPurchase', 'p', 'WITH', 'p.stock = s')
                    ->where('s.active = true')
                    ;
        
        $entities = $query->getQuery()
                          ->getResult()
                          ;

        return array('entities' => $entities);
    }
    
    /**
     * Shows Stock data.
     *
     * @Route("/{id}", name="es_finance_mystocks_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                     ->find($id)
                     ;
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $helper = new \ES\Finance\DatabaseBundle\QueriesEngine\QueryHelper();
        $helper->em = $this->getDoctrine();
        
        $date = date('Y-m-d', strtotime('-1 year'));
        
        $stock_events = $helper->getStockEvents($entity->getSymbol(), $date);
        $stock_prices = $helper->getHistoricalPrices($entity->getSymbol(), $date);
        
        return array('entity' => $entity,
                     'stock_prices' => $stock_prices,
                     'stock_events' => $stock_events
        );
    }
    
    public function stockStatusAction($stock_id) {
        $em = $this->getDoctrine()
                   ->getManager()
                   ;
        
        $query = $em->createQueryBuilder()
                    ->select('s')
                    ->from('ESFinanceDatabaseBundle:Stock', 's')
                    ->where('s.id = :stock_id')
                    ->setParameter('stock_id', $stock_id)
                    ;
        
        $entity = $query->getQuery()
                        ->getSingleResult()
                        ;
        
        $query = $em->createQueryBuilder()
                    ->select('hp')
                    ->from('ESFinanceDatabaseBundle:HistoricalPrice', 'hp')
                    ->where('hp.stock = :stock_id')
                    ->setParameter('stock_id', $stock_id)
                    ->orderBy('hp.date', 'DESC')
                    ->setMaxResults(5)
                    ;
        
        $prices = $query->getQuery()
                        ->getResult()
                        ;
        
        $close1 = 0.00;
        $close2 = 0.00;
        $close3 = 0.00;
        $close4 = 0.00;
        $close5 = 0.00;
        $color = '';
        $day_change = 0.00;
        $day_change_percentage = 0.00;
        $coin = 'USD';
        $badge1 = '';
        $badge2 = '';
        $badge3 = '';
        $badge4 = '';
        $badge5 = '';
        
        $i = 1;
        foreach ($prices as $price) {
            switch ($i) {
                case 1:
                    $close1 = $price->getClose();
                    break;
                case 2:
                    $day_change = $close1 - $price->getClose();
                    $day_change_percentage = ($day_change * 100) / $price->getClose();
                    $color = $day_change < 0 ? 'bg-lightRed' : 'bg-lightGreen';
                    $close2 = $price->getClose();
                    $badge1 = $close1 > $close2 ? 'bg-darkGreen' : 'bg-darkRed';
                    break;
                case 3:
                    $close3 = $price->getClose();
                    $badge2 = $close2 > $close3 ? 'bg-darkGreen' : 'bg-darkRed';
                    break;
                case 4:
                    $close4 = $price->getClose();
                    $badge3 = $close3 > $close4 ? 'bg-darkGreen' : 'bg-darkRed';
                    break;
                case 5:
                    $close5 = $price->getClose();
                    $badge4 = $close4 > $close5 ? 'bg-darkGreen' : 'bg-darkRed';
                    break;
            }
            
            $i++;
        }
        
        return $this->render('ESFinanceBackOfficeBundle:MyStocks:stock-status.html.twig', 
                             array('entity' => $entity,
                                   'size' => '',
                                   'color' => $color,
                                   'route' => true,
                                   'icon' => '',
                                   'close' => $close1,
                                   'day_change' => $day_change,
                                   'day_change_percentage' => $day_change_percentage,
                                   'coin' => $coin,
                                   'badge1' => $badge1,
                                   'badge2' => $badge2,
                                   'badge3' => $badge3,
                                   'badge4' => $badge4,
                                   'badge5' => $badge5
                                  )
                            );
    }
}
