<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ES\Finance\DatabaseBundle\Entity\StockPurchase;
use ES\Finance\DatabaseBundle\Form\StockPurchaseType;

/**
 * StockPurchase controller.
 *
 * @Route("/es_finance_stockspurchases")
 */
class StockPurchaseController extends Controller
{
    /**
     * Lists all StockPurchase entities.
     *
     * @Route("/", name="es_finance_stockspurchases")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getRepository('ESFinanceDatabaseBundle:StockPurchase');
        $query = $em->createQueryBuilder('p')
                    ->getQuery()
                    ;
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate($query,
                                         $request->query->get('page', 1)
                                        );
        
        return array('entities' => $entities);
    }

     /**
     * Creates a new StockPurchase entity.
     *
     * @Route("/", name="es_finance_stockspurchases_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:StockPurchase:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new StockPurchase();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_stockspurchases_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a StockPurchase entity.
     *
     * @param StockPurchase $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(StockPurchase $entity)
    {
        $form = $this->createForm(new StockPurchaseType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_stockspurchases_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_stockspurchases'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new StockPurchase entity.
     *
     * @Route("/new", name="es_finance_stockspurchases_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new StockPurchase();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a StockPurchase entity.
     *
     * @Route("/{id}", name="es_finance_stockspurchases_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:StockPurchase')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockPurchase entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing StockPurchase entity.
     *
     * @Route("/{id}/edit", name="es_finance_stockspurchases_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:StockPurchase')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockPurchase entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a StockPurchase entity.
     *
     * @param StockPurchase $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(StockPurchase $entity)
    {
        $form = $this->createForm(new StockPurchaseType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_stockspurchases_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_stockspurchases'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing StockPurchase entity.
     *
     * @Route("/{id}", name="es_finance_stockspurchases_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:StockPurchase:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:StockPurchase')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StockPurchase entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_stockspurchases_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a StockPurchase entity.
     *
     * @Route("/{id}", name="es_finance_stockspurchases_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:StockPurchase')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find StockPurchase entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_stockspurchases'));
    }

    /**
     * Creates a form to delete a StockPurchase entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_stockspurchases_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_stockspurchases'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
