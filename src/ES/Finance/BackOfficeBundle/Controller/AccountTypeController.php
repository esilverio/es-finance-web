<?php

namespace ES\Finance\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use ES\Finance\DatabaseBundle\Entity\AccountType;
use ES\Finance\DatabaseBundle\Form\AccountTypeType;

/**
 * AccountType controller.
 *
 * @Route("/es_finance_accounttypes")
 */
class AccountTypeController extends Controller
{
    /**
     * Lists all AccountType entities.
     *
     * @Route("/", name="es_finance_accounttypes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ESFinanceDatabaseBundle:AccountType')
                       ->findAll()
                       ;

        return array('entities' => $entities);
    }

     /**
     * Creates a new AccountType entity.
     *
     * @Route("/", name="es_finance_accounttypes_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:AccountType:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AccountType();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_accounttypes_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a AccountType entity.
     *
     * @param AccountType $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AccountType $entity)
    {
        $form = $this->createForm(new AccountTypeType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_accounttypes_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_accounttypes'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new AccountType entity.
     *
     * @Route("/new", name="es_finance_accounttypes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AccountType();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a AccountType entity.
     *
     * @Route("/{id}", name="es_finance_accounttypes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:AccountType')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AccountType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing AccountType entity.
     *
     * @Route("/{id}/edit", name="es_finance_accounttypes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:AccountType')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AccountType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a AccountType entity.
     *
     * @param AccountType $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(AccountType $entity)
    {
        $form = $this->createForm(new AccountTypeType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_accounttypes_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_accounttypes'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing AccountType entity.
     *
     * @Route("/{id}", name="es_finance_accounttypes_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:AccountType:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:AccountType')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AccountType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_accounttypes_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a AccountType entity.
     *
     * @Route("/{id}", name="es_finance_accounttypes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:AccountType')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AccountType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_accounttypes'));
    }

    /**
     * Creates a form to delete a AccountType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_accounttypes_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_accounttypes'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
