<?php

namespace ES\Finance\DatabaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StockSaleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('date', 'datePicker')
                ->add('stockPurchase')
                ->add('quantity')
                ->add('originalCoin')
                ->add('originalUnitPrice')
                ->add('originalExpenses')
                ->add('originalTotal')
                ->add('exchangeRate')
                ->add('homeCoin')
                ->add('homeUnitPrice')
                ->add('homeExpenses')
                ->add('homeTotal')
                ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ES\Finance\DatabaseBundle\Entity\StockSale'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'es_finance_databasebundle_stocksale';
    }
}
