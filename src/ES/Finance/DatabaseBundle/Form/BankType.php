<?php

namespace ES\Finance\DatabaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BankType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('abbreviation')
                ->add('name')
                ->add('code')
                ->add('swift')
                ->add('country')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ES\Finance\DatabaseBundle\Entity\Bank'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'es_finance_databasebundle_bank';
    }
}
