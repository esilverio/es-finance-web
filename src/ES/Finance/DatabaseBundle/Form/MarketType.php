<?php

namespace ES\Finance\DatabaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MarketType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name')
                ->add('countries', 'entity', array('class' => 'ESFinanceDatabaseBundle:Country',
                                                   'attr' => array('height' => '300px'),
                                                   'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                                                                      return $er->createQueryBuilder('s')
                                                                                ->where('s.active = TRUE')
                                                                                ->andWhere('s.visible = TRUE')
                                                                                ->orderBy('s.name', 'ASC');
                                                                      },
                                                    'expanded' => false,
                                                    'multiple' => true
                                                   )
                );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ES\Finance\DatabaseBundle\Entity\Market'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'es_finance_databasebundle_market';
    }
}
