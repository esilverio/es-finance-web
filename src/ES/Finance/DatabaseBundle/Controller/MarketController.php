<?php

namespace ES\Finance\DatabaseBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ES\Finance\DatabaseBundle\Entity\Market;
use ES\Finance\DatabaseBundle\Form\MarketType;

/**
 * Market controller.
 *
 * @Route("/es_finance_markets")
 */
class MarketController extends Controller
{
    /**
     * Lists all Market entities.
     *
     * @Route("/", name="es_finance_markets")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getRepository('ESFinanceDatabaseBundle:Market');
        $query = $em->createQueryBuilder('p')
                    ->getQuery()
                    ;
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate($query,
                                         $request->query->get('page', 1)
                                        );

        return array('entities' => $entities);
    }

     /**
     * Creates a new Market entity.
     *
     * @Route("/", name="es_finance_markets_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:Market:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Market();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_markets_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a Market entity.
     *
     * @param Market $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Market $entity)
    {
        $form = $this->createForm(new MarketType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_markets_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_markets'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new Market entity.
     *
     * @Route("/new", name="es_finance_markets_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Market();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a Market entity.
     *
     * @Route("/{id}", name="es_finance_markets_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Market')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Market entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Market entity.
     *
     * @Route("/{id}/edit", name="es_finance_markets_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Market')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Market entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a Market entity.
     *
     * @param Market $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Market $entity)
    {
        $form = $this->createForm(new MarketType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_markets_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_markets'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing Market entity.
     *
     * @Route("/{id}", name="es_finance_markets_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:Market:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Market')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Market entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_markets_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a Market entity.
     *
     * @Route("/{id}", name="es_finance_markets_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:Market')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Market entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_markets'));
    }

    /**
     * Creates a form to delete a Market entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_markets_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_markets'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
