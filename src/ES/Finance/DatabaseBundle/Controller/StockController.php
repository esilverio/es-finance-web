<?php

namespace ES\Finance\DatabaseBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ES\Finance\DatabaseBundle\Entity\Stock;
use ES\Finance\DatabaseBundle\Form\StockType;

/**
 * Stock controller.
 *
 * @Route("/es_finance_stocks")
 */
class StockController extends Controller
{
    /**
     * Lists all Stock entities.
     *
     * @Route("/", name="es_finance_stocks")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getRepository('ESFinanceDatabaseBundle:Stock');
        $query = $em->createQueryBuilder('p')
                    ->getQuery()
                    ;
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate($query,
                                         $request->query->get('page', 1)
                                        );

        return array('entities' => $entities);
    }

     /**
     * Creates a new Stock entity.
     *
     * @Route("/", name="es_finance_stocks_create")
     * @Method("POST")
     * @Template("ESFinanceDatabaseBundle:Stock:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Stock();
        $form = $this->createCreateForm($entity);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_stocks_show', 
                                   array('id' => $entity->getId())
                                  )
            );
        }

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a Stock entity.
     *
     * @param Stock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Stock $entity)
    {
        $form = $this->createForm(new StockType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_stocks_create'),
                                        'method' => 'POST',
                                 )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_stocks'
                                                            )
                                            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new Stock entity.
     *
     * @Route("/new", name="es_finance_stocks_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Stock();
        $form   = $this->createCreateForm($entity);

        return array('entity' => $entity,
                     'form'   => $form->createView()
        );
    }

    /**
     * Finds and displays a Stock entity.
     *
     * @Route("/{id}", name="es_finance_stocks_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array('entity' => $entity,
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Stock entity.
     *
     * @Route("/{id}/edit", name="es_finance_stocks_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a Stock entity.
     *
     * @param Stock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Stock $entity)
    {
        $form = $this->createForm(new StockType(), 
                                  $entity, 
                                  array('action' => $this->generateUrl('es_finance_stocks_update', array('id' => $entity->getId())),
                                        'method' => 'PUT'
                                       )
        );

        $form->add('submit', 'submit', array('label' => 'common.save',
                                             'attr' => array('class' => 'button',
                                                             'icon' => 'icon-floppy',
                                                             'route' => 'es_finance_stocks'
                                                            )
                                            )
        );

        return $form;
    }
    /**
     * Edits an existing Stock entity.
     *
     * @Route("/{id}", name="es_finance_stocks_update")
     * @Method("PUT")
     * @Template("ESFinanceDatabaseBundle:Stock:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                     ->find($id)
                     ;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Stock entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('es_finance_stocks_edit', 
                                                      array('id' => $id)
                                                     )
            );
        }

        return array('entity' => $entity,
                     'edit_form' => $editForm->createView(),
                     'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a Stock entity.
     *
     * @Route("/{id}", name="es_finance_stocks_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ESFinanceDatabaseBundle:Stock')
                         ->find($id)
                         ;

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Stock entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('es_finance_stocks'));
    }

    /**
     * Creates a form to delete a Stock entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('es_finance_stocks_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete',
                                                    'attr' => array('class' => 'button',
                                                                    'icon' => 'icon-remove',
                                                                    'route' => 'es_finance_stocks'
                                                                   )
                                                   )
                          )
                    ->getForm()
                    ;
    }
}
