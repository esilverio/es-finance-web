<?php

namespace ES\Finance\DatabaseBundle\QueriesEngine;

use ES\Finance\DatabaseBundle\Entity\StockEventProcessControl;

/**
 * Description of QueryHelper
 *
 * @author esilverio
 */
class QueryHelper {

    public $em;

    public function getStock($symbol, $active = true) {
        $em = $this->em;

        $builder = $em->getManager()
                      ->createQueryBuilder()
                      ->select('s')
                      ->from('ESFinanceDatabaseBundle:Stock', 's')
                      ->where('s.symbol = :symbol')
                      ->setParameter('symbol', $symbol)
                      ;

        if ($active) {
            $builder->where('s.active = :active')
                    ->setParameter('active', $active)
                    ;
        }
        
        $stock = $builder->getQuery()
                         ->getOneOrNullResult()
                         ;

        return $stock;
    }

    public function getStocks($symbol) {
        $em = $this->em;

        $query = $em->getManager()
                    ->createQueryBuilder()
                    ->select('s')
                    ->from('ESFinanceDatabaseBundle:Stock', 's')
                    ->where('s.active = true')
                    ;

        if ($symbol) {
            $query->andWhere('s.symbol = :symbol')
                  ->setParameter('symbol', $symbol)
                  ;
        }
        
        $stocks = $query->getQuery()
                        ->getResult()
                        ;

        return $stocks;
    }

    public function getStockEvents($symbol, $date) {
        $em = $this->em;

        $query = $em->getManager()
                    ->createQueryBuilder()
                    ->select('se')
                    ->from('ESFinanceDatabaseBundle:StockEvent', 'se')
                    ->innerJoin('se.stock', 's')
                    ->where('s.symbol = :symbol')
                    ->setParameter('symbol', $symbol)
                    ->orderBy('se.date', 'ASC')
                    ;
        
        if ($date) {
            $query->andWhere('se.date >= :date')
                  ->setParameter('date', $date)
                  ;
        }
        
        $stock_events = $query->getQuery()
                              ->getResult()
                              ;

        return $stock_events;
    }

    public function getStockEventType($event_type) {
        $em = $this->em;

        $builder = $em->getManager()
                      ->createQueryBuilder()
                      ->select('et')
                      ->from('ESFinanceDatabaseBundle:StockEventType', 'et')
                      ->where('et.id = :id')
                      ->setParameter('id', $event_type)
                      ;
        
        $stock_event_type = $builder->getQuery()
                                    ->getOneOrNullResult()
                                    ;

        return $stock_event_type;
    }
    
    public function get_rules($user, $stock) {
        $em = $this->em;

        $builder = $em->getManager()
                ->createQueryBuilder()
                ->select('r')
                ->from('ESFinanceDatabaseBundle:Rule', 'r')
                ->where('r.active = true')
        ;

        if ($user) {
            $builder->andWhere('r.user = :user');
            $builder->setParameter('user', $user);
        } else {
            $builder->andWhere('r.user is null');
        }

        if ($stock) {
            $builder->andWhere('r.stock = :stock');
            $builder->setParameter('stock', $stock);
        } else {
            $builder->andWhere('r.stock is null');
        }

        $rules = $builder->getQuery()
                ->getResult()
        ;

        return $rules;
    }

    public function getHistoricalPrices($stock, $date) {
        $em = $this->em;

        $query = $em->getManager()
                    ->createQueryBuilder()
                    ->select('hp')
                    ->from('ESFinanceDatabaseBundle:HistoricalPrice', 'hp')
                    ->innerJoin('hp.stock', 's')
                    ->where('s.symbol = :symbol')
                    ->setParameter('symbol', $stock)
                    ->orderBy('hp.date', 'ASC')
                    ;

        if ($date) {
            $query->andWhere('hp.date >= :date')
                  ->setParameter('date', $date)
                  ;
        }

        $hps = $query->getQuery()
                ->getResult()
        ;

        return $hps;
    }

    public function get_ema($stock, $date, $period) {
        $em = $this->em;

        $query = $em->getManager()
                ->createQueryBuilder()
                ->select('i')
                ->from('ESFinanceDatabaseBundle:Ema', 'i')
                ->innerJoin('i.stock', 's')
                ->where('s.symbol = :symbol')
                ->setParameter('symbol', $stock)
                ->andWhere('i.period = :period')
                ->setParameter('period', $period)
                ->andWhere('i.date = :date')
                ->setParameter('date', $date)
        ;

        $ema = $query->getQuery()
                ->getOneOrNullResult()
        ;

        $result = null;

        if ($ema) {
            $result = $ema->getValue();
        }

        return $result;
    }

    public function get_rsi($stock, $date, $period) {
        $em = $this->em;

        $query = $em->getManager()
                ->createQueryBuilder()
                ->select('i')
                ->from('ESFinanceDatabaseBundle:Rsi', 'i')
                ->innerJoin('i.stock', 's')
                ->where('s.symbol = :symbol')
                ->setParameter('symbol', $stock)
                ->andWhere('i.period = :period')
                ->setParameter('period', $period)
                ->andWhere('i.date = :date')
                ->setParameter('date', $date)
        ;

        $rsi = $query->getQuery()
                ->getOneOrNullResult()
        ;

        $result = null;

        if ($rsi) {
            $result = $rsi->getValue();
        }

        return $result;
    }

    public function get_sma($stock, $date, $period) {
        $em = $this->em;

        $query = $em->getManager()
                ->createQueryBuilder()
                ->select('i')
                ->from('ESFinanceDatabaseBundle:Sma', 'i')
                ->innerJoin('i.stock', 's')
                ->where('s.symbol = :symbol')
                ->setParameter('symbol', $stock)
                ->andWhere('i.period = :period')
                ->setParameter('period', $period)
                ->andWhere('i.date = :date')
                ->setParameter('date', $date)
        ;

        $sma = $query->getQuery()
                ->getOneOrNullResult()
        ;

        $result = null;

        if ($sma) {
            $result = $sma->getValue();
        }

        return $result;
    }

    public function get_william_r($stock, $date, $period) {
        $em = $this->em;

        $query = $em->getManager()
                ->createQueryBuilder()
                ->select('i')
                ->from('ESFinanceDatabaseBundle:WilliamR', 'i')
                ->innerJoin('i.stock', 's')
                ->where('s.symbol = :symbol')
                ->setParameter('symbol', $stock)
                ->andWhere('i.period = :period')
                ->setParameter('period', $period)
                ->andWhere('i.date = :date')
                ->setParameter('date', $date)
        ;

        $william_r = $query->getQuery()
                ->getOneOrNullResult()
        ;

        $result = null;

        if ($william_r) {
            $result = $william_r->getValue();
        }

        return $result;
    }

    public function getLastProcessedDate($symbol, $event_type) {
        $em = $this->em;

        $query = $em->getManager()
                    ->createQueryBuilder()
                    ->select('sepc')
                    ->from('ESFinanceDatabaseBundle:StockEventProcessControl', 'sepc')
                    ->innerJoin('sepc.stock', 's')
                    ->where('s.symbol = :symbol')
                    ->setParameter('symbol', $symbol)
                    ->andWhere('sepc.stockEventType = :stock_event_type')
                    ->setParameter('stock_event_type', $event_type)
                    ;

        $sepc = $query->getQuery()
                      ->getOneOrNullResult()
                      ;

        if ($sepc == null) {
            $stock = $this->getStock($symbol, null);
            $stock_event_type = $this->getStockEventType($event_type);
            
            $sepc = new StockEventProcessControl();

            $date = mktime(0, 0, 0, 1, 1, 1900);
            
            $sepc->setLastDayProcessed(date("Y-m-d", $date));
            $sepc->setStockEventType($stock_event_type);
            $sepc->setStock($stock);
        }

        return $sepc;
    }
}
