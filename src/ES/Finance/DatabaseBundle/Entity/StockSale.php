<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StockSale
 *
 * @ORM\Table(name="stocks_sales", indexes={@ORM\Index(name="fk_stocks_sales_1_idx", columns={"user"}), @ORM\Index(name="fk_stocks_sales_2_idx", columns={"stock_purchase"}), @ORM\Index(name="fk_stocks_sales_3_idx", columns={"original_coin"}), @ORM\Index(name="fk_stocks_sales_4_idx", columns={"home_coin"})})
 * @ORM\Entity
 */
class StockSale
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="smallint", nullable=false)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="original_unit_price", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $originalUnitPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="original_expenses", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $originalExpenses;

    /**
     * @var string
     *
     * @ORM\Column(name="original_total", type="decimal", precision=20, scale=4, nullable=false)
     */
    private $originalTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="exchange_rate", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $exchangeRate;

    /**
     * @var string
     *
     * @ORM\Column(name="home_unit_price", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $homeUnitPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="home_expenses", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $homeExpenses;

    /**
     * @var string
     *
     * @ORM\Column(name="home_total", type="decimal", precision=20, scale=4, nullable=false)
     */
    private $homeTotal;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var \StockPurchase
     *
     * @ORM\ManyToOne(targetEntity="StockPurchase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock_purchase", referencedColumnName="id", nullable=false)
     * })
     */
    private $stockPurchase;

    /**
     * @var \Coin
     *
     * @ORM\ManyToOne(targetEntity="Coin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="original_coin", referencedColumnName="id", nullable=false)
     * })
     */
    private $originalCoin;

    /**
     * @var \Coin
     *
     * @ORM\ManyToOne(targetEntity="Coin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="home_coin", referencedColumnName="id", nullable=false)
     * })
     */
    private $homeCoin;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return StockSale
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return StockSale
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set originalUnitPrice
     *
     * @param string $originalUnitPrice
     * @return StockSale
     */
    public function setOriginalUnitPrice($originalUnitPrice)
    {
        $this->originalUnitPrice = $originalUnitPrice;

        return $this;
    }

    /**
     * Get originalUnitPrice
     *
     * @return string 
     */
    public function getOriginalUnitPrice()
    {
        return $this->originalUnitPrice;
    }

    /**
     * Set originalExpenses
     *
     * @param string $originalExpenses
     * @return StockSale
     */
    public function setOriginalExpenses($originalExpenses)
    {
        $this->originalExpenses = $originalExpenses;

        return $this;
    }

    /**
     * Get originalExpenses
     *
     * @return string 
     */
    public function getOriginalExpenses()
    {
        return $this->originalExpenses;
    }

    /**
     * Set originalTotal
     *
     * @param string $originalTotal
     * @return StockSale
     */
    public function setOriginalTotal($originalTotal)
    {
        $this->originalTotal = $originalTotal;

        return $this;
    }

    /**
     * Get originalTotal
     *
     * @return string 
     */
    public function getOriginalTotal()
    {
        return $this->originalTotal;
    }

    /**
     * Set exchangeRate
     *
     * @param string $exchangeRate
     * @return StockSale
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return string 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set homeUnitPrice
     *
     * @param string $homeUnitPrice
     * @return StockSale
     */
    public function setHomeUnitPrice($homeUnitPrice)
    {
        $this->homeUnitPrice = $homeUnitPrice;

        return $this;
    }

    /**
     * Get homeUnitPrice
     *
     * @return string 
     */
    public function getHomeUnitPrice()
    {
        return $this->homeUnitPrice;
    }

    /**
     * Set homeExpenses
     *
     * @param string $homeExpenses
     * @return StockSale
     */
    public function setHomeExpenses($homeExpenses)
    {
        $this->homeExpenses = $homeExpenses;

        return $this;
    }

    /**
     * Get homeExpenses
     *
     * @return string 
     */
    public function getHomeExpenses()
    {
        return $this->homeExpenses;
    }

    /**
     * Set homeTotal
     *
     * @param string $homeTotal
     * @return StockSale
     */
    public function setHomeTotal($homeTotal)
    {
        $this->homeTotal = $homeTotal;

        return $this;
    }

    /**
     * Get homeTotal
     *
     * @return string 
     */
    public function getHomeTotal()
    {
        return $this->homeTotal;
    }

    /**
     * Set user
     *
     * @param \ES\Finance\DatabaseBundle\Entity\User $user
     * @return StockSale
     */
    public function setUser(\ES\Finance\DatabaseBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ES\Finance\DatabaseBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set stockPurchase
     *
     * @param \ES\Finance\DatabaseBundle\Entity\StockPurchase $stockPurchase
     * @return StockSale
     */
    public function setStockPurchase(\ES\Finance\DatabaseBundle\Entity\StockPurchase $stockPurchase)
    {
        $this->stockPurchase = $stockPurchase;

        return $this;
    }

    /**
     * Get stockPurchase
     *
     * @return \ES\Finance\DatabaseBundle\Entity\StockPurchase 
     */
    public function getStockPurchase()
    {
        return $this->stockPurchase;
    }

    /**
     * Set originalCoin
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Coin $originalCoin
     * @return StockSale
     */
    public function setOriginalCoin(\ES\Finance\DatabaseBundle\Entity\Coin $originalCoin)
    {
        $this->originalCoin = $originalCoin;

        return $this;
    }

    /**
     * Get originalCoin
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Coin 
     */
    public function getOriginalCoin()
    {
        return $this->originalCoin;
    }

    /**
     * Set homeCoin
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Coin $homeCoin
     * @return StockSale
     */
    public function setHomeCoin(\ES\Finance\DatabaseBundle\Entity\Coin $homeCoin)
    {
        $this->homeCoin = $homeCoin;

        return $this;
    }

    /**
     * Get homeCoin
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Coin 
     */
    public function getHomeCoin()
    {
        return $this->homeCoin;
    }
}
