<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoricalPrice
 *
 * @ORM\Table(name="historical_prices", indexes={@ORM\Index(name="fk_historical_prices_1_idx", columns={"stock"})})
 * @ORM\Entity
 */
class HistoricalPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="open", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $open;

    /**
     * @var string
     *
     * @ORM\Column(name="high", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $high;

    /**
     * @var string
     *
     * @ORM\Column(name="low", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $low;

    /**
     * @var string
     *
     * @ORM\Column(name="close", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $close;

    /**
     * @var boolean
     *
     * @ORM\Column(name="up", type="boolean", nullable=false)
     */
    private $up;

    /**
     * @var boolean
     *
     * @ORM\Column(name="down", type="boolean", nullable=false)
     */
    private $down;

    /**
     * @var string
     *
     * @ORM\Column(name="increase", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $increase;

    /**
     * @var string
     *
     * @ORM\Column(name="decrease", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $decrease;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume", type="bigint", nullable=false)
     */
    private $volume;

    /**
     * @var string
     *
     * @ORM\Column(name="adjusted_close", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $adjustedClose;

    /**
     * @var \Stock
     *
     * @ORM\ManyToOne(targetEntity="Stock")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock", referencedColumnName="id", nullable=false)
     * })
     */
    private $stock;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return HistoricalPrice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set open
     *
     * @param string $open
     * @return HistoricalPrice
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return string 
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * Set high
     *
     * @param string $high
     * @return HistoricalPrice
     */
    public function setHigh($high)
    {
        $this->high = $high;

        return $this;
    }

    /**
     * Get high
     *
     * @return string 
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * Set low
     *
     * @param string $low
     * @return HistoricalPrice
     */
    public function setLow($low)
    {
        $this->low = $low;

        return $this;
    }

    /**
     * Get low
     *
     * @return string 
     */
    public function getLow()
    {
        return $this->low;
    }

    /**
     * Set close
     *
     * @param string $close
     * @return HistoricalPrice
     */
    public function setClose($close)
    {
        $this->close = $close;

        return $this;
    }

    /**
     * Get close
     *
     * @return string 
     */
    public function getClose()
    {
        return $this->close;
    }

    /**
     * Set up
     *
     * @param boolean $up
     * @return HistoricalPrice
     */
    public function setUp($up)
    {
        $this->up = $up;

        return $this;
    }

    /**
     * Get up
     *
     * @return boolean 
     */
    public function getUp()
    {
        return $this->up;
    }

    /**
     * Set down
     *
     * @param boolean $down
     * @return HistoricalPrice
     */
    public function setDown($down)
    {
        $this->down = $down;

        return $this;
    }

    /**
     * Get down
     *
     * @return boolean 
     */
    public function getDown()
    {
        return $this->down;
    }

    /**
     * Set increase
     *
     * @param string $increase
     * @return HistoricalPrice
     */
    public function setIncrease($increase)
    {
        $this->increase = $increase;

        return $this;
    }

    /**
     * Get increase
     *
     * @return string 
     */
    public function getIncrease()
    {
        return $this->increase;
    }

    /**
     * Set decrease
     *
     * @param string $decrease
     * @return HistoricalPrice
     */
    public function setDecrease($decrease)
    {
        $this->decrease = $decrease;

        return $this;
    }

    /**
     * Get decrease
     *
     * @return string 
     */
    public function getDecrease()
    {
        return $this->decrease;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     * @return HistoricalPrice
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return integer 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set adjustedClose
     *
     * @param string $adjustedClose
     * @return HistoricalPrice
     */
    public function setAdjustedClose($adjustedClose)
    {
        $this->adjustedClose = $adjustedClose;

        return $this;
    }

    /**
     * Get adjustedClose
     *
     * @return string 
     */
    public function getAdjustedClose()
    {
        return $this->adjustedClose;
    }

    /**
     * Set stock
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Stock $stock
     * @return HistoricalPrice
     */
    public function setStock(\ES\Finance\DatabaseBundle\Entity\Stock $stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Stock 
     */
    public function getStock()
    {
        return $this->stock;
    }
}
