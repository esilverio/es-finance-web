<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StockEvent
 *
 * @ORM\Table(name="stocks_events", indexes={@ORM\Index(name="fk_stocks_events_1_idx", columns={"stock"}), @ORM\Index(name="fk_stocks_events_1_idx", columns={"stock_event_type"})})
 * @ORM\Entity
 */
class StockEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bearish", type="boolean", nullable=false)
     */
    private $bearish;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bullish", type="boolean", nullable=false)
     */
    private $bullish;

    /**
     * @var \Stock
     *
     * @ORM\ManyToOne(targetEntity="Stock")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock", referencedColumnName="id", nullable=false)
     * })
     */
    private $stock;

    /**
     * @var \StockEventType
     *
     * @ORM\ManyToOne(targetEntity="StockEventType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock_event_type", referencedColumnName="id", nullable=false)
     * })
     */
    private $stockEventType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return StockEvent
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set bearish
     *
     * @param boolean $bearish
     * @return StockEvent
     */
    public function setBearish($bearish)
    {
        $this->bearish = $bearish;

        return $this;
    }

    /**
     * Get bearish
     *
     * @return boolean 
     */
    public function getBearish()
    {
        return $this->bearish;
    }

    /**
     * Set bullish
     *
     * @param boolean $bullish
     * @return StockEvent
     */
    public function setBullish($bullish)
    {
        $this->bullish = $bullish;

        return $this;
    }

    /**
     * Get bullish
     *
     * @return boolean 
     */
    public function getBullish()
    {
        return $this->bullish;
    }

    /**
     * Set stock
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Stock $stock
     * @return StockEvent
     */
    public function setStock(\ES\Finance\DatabaseBundle\Entity\Stock $stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set stockEventType
     *
     * @param \ES\Finance\DatabaseBundle\Entity\StockEventType $stockEventType
     * @return StockEvent
     */
    public function setStockEventType(\ES\Finance\DatabaseBundle\Entity\StockEventType $stockEventType)
    {
        $this->stockEventType = $stockEventType;

        return $this;
    }

    /**
     * Get stockEventType
     *
     * @return \ES\Finance\DatabaseBundle\Entity\StockEventType 
     */
    public function getStockEventType()
    {
        return $this->stockEventType;
    }
}
