<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menu
 *
 * @ORM\Table(name="menus", indexes={@ORM\Index(name="fk_menus_1_idx", columns={"menu_group"})})
 * @ORM\Entity
 */
class Menu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip", type="string", length=150, nullable=true)
     */
    private $tooltip;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=50, nullable=false)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=25, nullable=false)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="style", type="string", length=80, nullable=false)
     */
    private $style;

    /**
     * @var integer
     *
     * @ORM\Column(name="order", type="smallint", nullable=false)
     */
    private $order;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \MenuGroup
     *
     * @ORM\ManyToOne(targetEntity="MenuGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_group", referencedColumnName="id", nullable=false)
     * })
     */
    private $menuGroup;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Profile", inversedBy="menus")
     * @ORM\JoinTable(name="menus_profiles",
     *   joinColumns={
     *     @ORM\JoinColumn(name="menu", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="profile", referencedColumnName="id")
     *   }
     * )
     */
    private $profiles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->profiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set tooltip
     *
     * @param string $tooltip
     * @return Menu
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * Get tooltip
     *
     * @return string 
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return Menu
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Menu
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set style
     *
     * @param string $style
     * @return Menu
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return string 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Menu
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Menu
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set menuGroup
     *
     * @param \ES\Finance\DatabaseBundle\Entity\MenuGroup $menuGroup
     * @return Menu
     */
    public function setMenuGroup(\ES\Finance\DatabaseBundle\Entity\MenuGroup $menuGroup)
    {
        $this->menuGroup = $menuGroup;

        return $this;
    }

    /**
     * Get menuGroup
     *
     * @return \ES\Finance\DatabaseBundle\Entity\MenuGroup 
     */
    public function getMenuGroup()
    {
        return $this->menuGroup;
    }

    /**
     * Add profile
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Profile $profile
     * @return Menu
     */
    public function addProfile(\ES\Finance\DatabaseBundle\Entity\Profile $profile)
    {
        $this->profiles[] = $profile;

        return $this;
    }

    /**
     * Remove profile
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Profile $profile
     */
    public function removeProfile(\ES\Finance\DatabaseBundle\Entity\Profile $profile)
    {
        $this->profiles->removeElement($profile);
    }

    /**
     * Get profiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfiles()
    {
        return $this->profiles;
    }
}
