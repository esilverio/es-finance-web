<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StockEventType
 *
 * @ORM\Table(name="stocks_events_types", indexes={@ORM\Index(name="fk_stocks_events_types_1_idx", columns={"stock_event_group"})})
 * @ORM\Entity
 */
class StockEventType
{
    const CANDELSTICKS_HAMMER = 1;
    const CANDELSTICKS_INVERTED_HAMMER = 2;
    const CANDELSTICKS_HANGING_MAN = 3;
    const CANDELSTICKS_SHOOTING_STAR = 4;
    const CANDELSTICKS_ENGULFING = 5;
    const CANDELSTICKS_HARAMI = 6;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="period", type="smallint", nullable=false)
     */
    private $period;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bearish", type="boolean", nullable=false)
     */
    private $bearish;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bullish", type="boolean", nullable=false)
     */
    private $bullish;

    /**
     * @var \StockEventGroup
     *
     * @ORM\ManyToOne(targetEntity="StockEventGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock_event_group", referencedColumnName="id", nullable=false)
     * })
     */
    private $stockEventGroup;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return StockEventType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set period
     *
     * @param integer $period
     * @return StockEventType
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return integer 
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set bearish
     *
     * @param boolean $bearish
     * @return StockEventType
     */
    public function setBearish($bearish)
    {
        $this->bearish = $bearish;

        return $this;
    }

    /**
     * Get bearish
     *
     * @return boolean 
     */
    public function getBearish()
    {
        return $this->bearish;
    }

    /**
     * Set bullish
     *
     * @param boolean $bullish
     * @return StockEventType
     */
    public function setBullish($bullish)
    {
        $this->bullish = $bullish;

        return $this;
    }

    /**
     * Get bullish
     *
     * @return boolean 
     */
    public function getBullish()
    {
        return $this->bullish;
    }

    /**
     * Set stockEventGroup
     *
     * @param \ES\Finance\DatabaseBundle\Entity\StockEventGroup $stockEventGroup
     * @return StockEventType
     */
    public function setStockEventGroup(\ES\Finance\DatabaseBundle\Entity\StockEventGroup $stockEventGroup)
    {
        $this->stockEventGroup = $stockEventGroup;

        return $this;
    }

    /**
     * Get stockEventGroup
     *
     * @return \ES\Finance\DatabaseBundle\Entity\StockEventGroup 
     */
    public function getStockEventGroup()
    {
        return $this->stockEventGroup;
    }
}
