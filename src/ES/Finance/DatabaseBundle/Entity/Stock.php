<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stock
 *
 * @ORM\Table(name="stocks", uniqueConstraints={@ORM\UniqueConstraint(name="uk_stocks_1_idx", columns={"symbol"})}, indexes={@ORM\Index(name="fk_stocks_1_idx", columns={"country"}), @ORM\Index(name="fk_stocks_2_idx", columns={"coin"}), @ORM\Index(name="fk_stocks_3_idx", columns={"sector"}), @ORM\Index(name="fk_stocks_4_idx", columns={"industry"})})
 * @ORM\Entity
 */
class Stock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=10, nullable=false)
     */
    private $symbol;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=80, nullable=false)
     */
    private $companyName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Sector
     *
     * @ORM\ManyToOne(targetEntity="Sector")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sector", referencedColumnName="id", nullable=false)
     * })
     */
    private $sector;

    /**
     * @var \Industry
     *
     * @ORM\ManyToOne(targetEntity="Industry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="industry", referencedColumnName="id", nullable=false)
     * })
     */
    private $industry;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     * })
     */
    private $country;

    /**
     * @var \Coin
     *
     * @ORM\ManyToOne(targetEntity="Coin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coin", referencedColumnName="id", nullable=false)
     * })
     */
    private $coin;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Index", mappedBy="stocks")
     */
    private $indexes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->indexes = new \Doctrine\Common\Collections\ArrayCollection();
    }
 
    /**
     * Sector name
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->symbol . " - " . $this->companyName;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return Stock
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Stock
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Stock
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set sector
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Sector $sector
     * @return Stock
     */
    public function setSector(\ES\Finance\DatabaseBundle\Entity\Sector $sector)
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * Get sector
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Sector 
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Set industry
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Industry $industry
     * @return Stock
     */
    public function setIndustry(\ES\Finance\DatabaseBundle\Entity\Industry $industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get industry
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Industry 
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set country
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Country $country
     * @return Stock
     */
    public function setCountry(\ES\Finance\DatabaseBundle\Entity\Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set coin
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Coin $coin
     * @return Stock
     */
    public function setCoin(\ES\Finance\DatabaseBundle\Entity\Coin $coin)
    {
        $this->coin = $coin;

        return $this;
    }

    /**
     * Get coin
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Coin 
     */
    public function getCoin()
    {
        return $this->coin;
    }

    /**
     * Add index
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Index $index
     * @return Stock
     */
    public function addIndex(\ES\Finance\DatabaseBundle\Entity\Index $index)
    {
        $this->indexes[] = $index;

        return $this;
    }

    /**
     * Remove index
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Index $index
     */
    public function removeIndex(\ES\Finance\DatabaseBundle\Entity\Index $index)
    {
        $this->indexes->removeElement($index);
    }

    /**
     * Get indexes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIndexes()
    {
        return $this->indexes;
    }
}
