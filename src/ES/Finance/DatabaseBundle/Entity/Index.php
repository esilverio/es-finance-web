<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Index
 *
 * @ORM\Table(name="indexes", uniqueConstraints={@ORM\UniqueConstraint(name="uk_indexes_1_idx", columns={"symbol"})}, indexes={@ORM\Index(name="fk_indexes_1_idx", columns={"country"})})
 * @ORM\Entity
 */
class Index
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=10, nullable=false)
     */
    private $symbol;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     * })
     */
    private $country;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Stock", inversedBy="indexes")
     * @ORM\JoinTable(name="stocks_indexes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="stock", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="index", referencedColumnName="id")
     *   }
     * )
     */
    private $stocks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stocks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return Index
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Index
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Country $country
     * @return Index
     */
    public function setCountry(\ES\Finance\DatabaseBundle\Entity\Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add stock
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Stock $stock
     * @return Index
     */
    public function addStock(\ES\Finance\DatabaseBundle\Entity\Stock $stock)
    {
        $this->stocks[] = $stock;

        return $this;
    }

    /**
     * Remove stock
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Stock $stock
     */
    public function removeStock(\ES\Finance\DatabaseBundle\Entity\Stock $stock)
    {
        $this->stocks->removeElement($stock);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStocks()
    {
        return $this->stocks;
    }
}
