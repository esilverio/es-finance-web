<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="accounts", indexes={@ORM\Index(name="fk_accounts_1_idx", columns={"bank"}), @ORM\Index(name="fk_accounts_3_idx", columns={"user"}), @ORM\Index(name="fk_accounts_2_idx", columns={"account_type"}), @ORM\Index(name="fk_accounts_4_idx", columns={"coin"})})
 * @ORM\Entity
 */
class Account
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=50, nullable=true)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="opening_date", type="date", nullable=false)
     */
    private $openingDate;

    /**
     * @var string
     *
     * @ORM\Column(name="opening_balance", type="decimal", precision=12, scale=2, nullable=false)
     */
    private $openingBalance;

    /**
     * @var string
     *
     * @ORM\Column(name="nib", type="string", length=21, nullable=true)
     */
    private $nib;

    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=25, nullable=true)
     */
    private $iban;

    /**
     * @var \Bank
     *
     * @ORM\ManyToOne(targetEntity="Bank")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bank", referencedColumnName="id", nullable=false)
     * })
     */
    private $bank;

    /**
     * @var \AccountType
     *
     * @ORM\ManyToOne(targetEntity="AccountType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_type", referencedColumnName="id", nullable=false)
     * })
     */
    private $accountType;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var \Coin
     *
     * @ORM\ManyToOne(targetEntity="Coin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coin", referencedColumnName="id", nullable=false)
     * })
     */
    private $coin;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Account
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set openingDate
     *
     * @param \DateTime $openingDate
     * @return Account
     */
    public function setOpeningDate($openingDate)
    {
        $this->openingDate = $openingDate;

        return $this;
    }

    /**
     * Get openingDate
     *
     * @return \DateTime 
     */
    public function getOpeningDate()
    {
        return $this->openingDate;
    }

    /**
     * Set openingBalance
     *
     * @param string $openingBalance
     * @return Account
     */
    public function setOpeningBalance($openingBalance)
    {
        $this->openingBalance = $openingBalance;

        return $this;
    }

    /**
     * Get openingBalance
     *
     * @return string 
     */
    public function getOpeningBalance()
    {
        return $this->openingBalance;
    }

    /**
     * Set nib
     *
     * @param string $nib
     * @return Account
     */
    public function setNib($nib)
    {
        $this->nib = $nib;

        return $this;
    }

    /**
     * Get nib
     *
     * @return string 
     */
    public function getNib()
    {
        return $this->nib;
    }

    /**
     * Set iban
     *
     * @param string $iban
     * @return Account
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string 
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bank
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Bank $bank
     * @return Account
     */
    public function setBank(\ES\Finance\DatabaseBundle\Entity\Bank $bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Bank 
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set accountType
     *
     * @param \ES\Finance\DatabaseBundle\Entity\AccountType $accountType
     * @return Account
     */
    public function setAccountType(\ES\Finance\DatabaseBundle\Entity\AccountType $accountType)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get accountType
     *
     * @return \ES\Finance\DatabaseBundle\Entity\AccountType 
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Set user
     *
     * @param \ES\Finance\DatabaseBundle\Entity\User $user
     * @return Account
     */
    public function setUser(\ES\Finance\DatabaseBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ES\Finance\DatabaseBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set coin
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Coin $coin
     * @return Account
     */
    public function setCoin(\ES\Finance\DatabaseBundle\Entity\Coin $coin)
    {
        $this->coin = $coin;

        return $this;
    }

    /**
     * Get coin
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Coin 
     */
    public function getCoin()
    {
        return $this->coin;
    }
}
