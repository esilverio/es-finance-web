<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StockEventProcessControl
 *
 * @ORM\Table(name="stocks_events_process_control", indexes={@ORM\Index(name="fk_stocks_events_process_control_1_idx", columns={"stock"}), @ORM\Index(name="fk_stocks_events_process_control_2_idx", columns={"stock_event_type"})})
 * @ORM\Entity
 */
class StockEventProcessControl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_day_processed", type="datetime", nullable=false)
     */
    private $lastDayProcessed;

    /**
     * @var \Stock
     *
     * @ORM\ManyToOne(targetEntity="Stock")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock", referencedColumnName="id", nullable=false)
     * })
     */
    private $stock;

    /**
     * @var \StockEventType
     *
     * @ORM\ManyToOne(targetEntity="StockEventType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock_event_type", referencedColumnName="id", nullable=false)
     * })
     */
    private $stockEventType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastDayProcessed
     *
     * @param \DateTime $lastDayProcessed
     * @return StockEventProcessControl
     */
    public function setLastDayProcessed($lastDayProcessed)
    {
        $this->lastDayProcessed = $lastDayProcessed;

        return $this;
    }

    /**
     * Get lastDayProcessed
     *
     * @return \DateTime 
     */
    public function getLastDayProcessed()
    {
        return $this->lastDayProcessed;
    }

    /**
     * Set stock
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Stock $stock
     * @return StockEventProcessControl
     */
    public function setStock(\ES\Finance\DatabaseBundle\Entity\Stock $stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Stock 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set stockEventType
     *
     * @param \ES\Finance\DatabaseBundle\Entity\StockEventType $stockEventType
     * @return StockEventProcessControl
     */
    public function setStockEventType(\ES\Finance\DatabaseBundle\Entity\StockEventType $stockEventType)
    {
        $this->stockEventType = $stockEventType;

        return $this;
    }

    /**
     * Get stockEventType
     *
     * @return \ES\Finance\DatabaseBundle\Entity\StockEventType 
     */
    public function getStockEventType()
    {
        return $this->stockEventType;
    }
}
