<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountIncome
 *
 * @ORM\Table(name="accounts_incomes", indexes={@ORM\Index(name="fk_acounts_incomes_1_idx", columns={"account"}), @ORM\Index(name="fk_accounts_incomes_2_idx", columns={"original_coin"})})
 * @ORM\Entity
 */
class AccountIncome
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=120, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="original_total", type="decimal", precision=20, scale=4, nullable=false)
     */
    private $originalTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="exchange_rate", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $exchangeRate;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=20, scale=4, nullable=false)
     */
    private $total;

    /**
     * @var \Coin
     *
     * @ORM\ManyToOne(targetEntity="Coin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="original_coin", referencedColumnName="id", nullable=false)
     * })
     */
    private $originalCoin;

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account", referencedColumnName="id", nullable=false)
     * })
     */
    private $account;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return AccountIncome
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AccountIncome
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set originalTotal
     *
     * @param string $originalTotal
     * @return AccountIncome
     */
    public function setOriginalTotal($originalTotal)
    {
        $this->originalTotal = $originalTotal;

        return $this;
    }

    /**
     * Get originalTotal
     *
     * @return string 
     */
    public function getOriginalTotal()
    {
        return $this->originalTotal;
    }

    /**
     * Set exchangeRate
     *
     * @param string $exchangeRate
     * @return AccountIncome
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return string 
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return AccountIncome
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set originalCoin
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Coin $originalCoin
     * @return AccountIncome
     */
    public function setOriginalCoin(\ES\Finance\DatabaseBundle\Entity\Coin $originalCoin)
    {
        $this->originalCoin = $originalCoin;

        return $this;
    }

    /**
     * Get originalCoin
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Coin 
     */
    public function getOriginalCoin()
    {
        return $this->originalCoin;
    }

    /**
     * Set account
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Account $account
     * @return AccountIncome
     */
    public function setAccount(\ES\Finance\DatabaseBundle\Entity\Account $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
