<?php

namespace ES\Finance\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coin
 *
 * @ORM\Table(name="coins", uniqueConstraints={@ORM\UniqueConstraint(name="uk_coins_1_idx", columns={"symbol"})}, indexes={@ORM\Index(name="fk_coins_1_idx", columns={"country"})})
 * @ORM\Entity
 */
class Coin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=5, nullable=false)
     */
    private $symbol;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     * })
     */
    private $country;

    /**
     * Coin name
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return Coin
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string 
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Coin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param \ES\Finance\DatabaseBundle\Entity\Country $country
     * @return Coin
     */
    public function setCountry(\ES\Finance\DatabaseBundle\Entity\Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \ES\Finance\DatabaseBundle\Entity\Countries 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
